package com.rahnema.gram;

/**
 * Created by farbod on 6/28/2017 AD.
 */

public class MediaManager {
    public static final String MEDIA_PLAY_PAUSE_ACTION = "menbar.mediamanager.action.play_pause";
    public static final String MEDIA_FAST_FORWARD_ACTION = "menbar.mediamanager.action.fast_forward";
    public static final String MEDIA_REWIND_ACTION = "menbar.mediamanager.action.rewind";
    public static final String MEDIA_START_ACTION = "menbar.mediamanager.action.start";
    public static final String MEDIA_STOP_ACTION = "menbar.mediamanager.action.stop";

    public static final String MEDIA_ID_EXTRA = "menbar.mediamanager.extra.id";
    public static final String MEDIA_PLAY_EXTRA = "menbar.mediamanager.extra.playing";

    public static final int MEDIA_NOTIFICATION_ID = 123456;
}