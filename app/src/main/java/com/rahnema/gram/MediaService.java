package com.rahnema.gram;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Binder;
import android.os.Build;
import android.os.IBinder;
import android.os.PowerManager;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.widget.RemoteViews;

import com.squareup.picasso.Picasso;

/**
 * Created by Farbod on 1/18/2016.
 */
public class MediaService extends Service implements
        MediaPlayer.OnPreparedListener,
        MediaPlayer.OnErrorListener,
        MediaPlayer.OnCompletionListener,
        AudioManager.OnAudioFocusChangeListener {
    private static final String TAG = MediaService.class.getSimpleName();

    private final IBinder mBinder = new MediaBinder();

    private MediaPlayer mPlayer;
    private Song mSong;
    private boolean mIsPlayed;
    private boolean mIsStoped;

    private NotificationCompat.Builder mCurrentNotificationCompat;
    private NotificationManager mNotificationManager;

    private AudioManager mAudioManager;
    private LocalBroadcastManager mBroadcastManager;

    public MediaService() {
        // nothing!
    }

    @Override
    public void onCreate() {
        super.onCreate();

        mNotificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        mCurrentNotificationCompat = new NotificationCompat.Builder(this);
        mBroadcastManager = LocalBroadcastManager.getInstance(this);
        mAudioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        mAudioManager.requestAudioFocus(this, AudioManager.STREAM_MUSIC, AudioManager.AUDIOFOCUS_GAIN);

        initMediaPlayer();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        handleIntent(intent);

        return START_NOT_STICKY;
    }

    private void handleIntent(Intent intent) {
        if (intent != null && intent.getAction() != null) {
            if (intent.getAction().equalsIgnoreCase(MediaManager.MEDIA_PLAY_PAUSE_ACTION)) {
                if (isPlaying()) {
                    pausePlayer();
                } else {
                    startPlayer();
                }
            } else if (intent.getAction().equalsIgnoreCase(MediaManager.MEDIA_FAST_FORWARD_ACTION)) {
                fastForward();
            } else if (intent.getAction().equalsIgnoreCase(MediaManager.MEDIA_REWIND_ACTION)) {
                fastRewind();
            } else if (intent.getAction().equalsIgnoreCase(MediaManager.MEDIA_STOP_ACTION)) {
                stopPlayer();
            } else if (intent.getAction().equalsIgnoreCase(MediaManager.MEDIA_START_ACTION)) {
                playSong();
            }
        }
    }

    @Override
    public void onLowMemory() {
        // TODO - magic stuff here
    }

    @Override
    public void onDestroy() {
        mAudioManager.abandonAudioFocus(this);
        if (mPlayer != null) {
            if (mPlayer.isPlaying()) {
                mPlayer.stop();
            }
            mPlayer.reset();
            mPlayer.release();
        }
        mPlayer = null;
        mIsStoped = true;

        super.onDestroy();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    @Override
    public void onAudioFocusChange(int focusChange) {
        switch (focusChange) {
            case AudioManager.AUDIOFOCUS_GAIN:
                // resume playback
                if (mPlayer == null) initMediaPlayer();
                //else if (!mPlayer.isPlaying()) startPlayer();
                mPlayer.setVolume(1.0f, 1.0f);
                break;

            case AudioManager.AUDIOFOCUS_LOSS:
                // Lost focus for an unbounded amount of time: stop playback and release media player
                if (mPlayer.isPlaying()) {
                    pausePlayer();
                }
                break;

            case AudioManager.AUDIOFOCUS_LOSS_TRANSIENT:
                // Lost focus for a short time, but we have to stop
                // playback. We don't release the media player because playback
                // is likely to resume
                if (mPlayer.isPlaying()) {
                    pausePlayer();
                }
                break;

            case AudioManager.AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK:
                // Lost focus for a short time, but it's ok to keep playing
                // at an attenuated level
                if (mPlayer.isPlaying()) mPlayer.setVolume(0.1f, 0.1f);
                break;
        }
    }

    public class MediaBinder extends Binder {
        public MediaService getService() {
            return MediaService.this;
        }
    }

    @Override
    public void onCompletion(MediaPlayer mp) {
        // Player
        try {
            if (mp.isPlaying()) {
                mp.pause();
            }
            mp.stop();
        } catch (IllegalStateException e) {
            e.printStackTrace();
        }
        mp.reset();

        mIsPlayed = false;
        mIsStoped = true;

        stopForeground(true);
        stopSelf();
    }

    @Override
    public boolean onError(MediaPlayer mp, int what, int extra) {
        // Player
        mp.reset();

        mIsPlayed = false;
        mIsStoped = true;

        stopForeground(true);
        stopSelf();

        return false;
    }

    @Override
    public void onPrepared(MediaPlayer mp) {
        // Player
        mp.start();

        mIsPlayed = true;
        mIsStoped = false;

        // Notification
        showMediaNotification();

        Intent i = new Intent();
        i.setAction(MediaManager.MEDIA_START_ACTION);
        mBroadcastManager.sendBroadcast(i);
    }

    private void initMediaPlayer() {
        mPlayer = new MediaPlayer();
        mPlayer.setWakeMode(getApplicationContext(), PowerManager.PARTIAL_WAKE_LOCK);
        mPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
        mPlayer.setOnPreparedListener(this);
        mPlayer.setOnCompletionListener(this);
        mPlayer.setOnErrorListener(this);
    }

    private void showMediaNotification() {
        Intent notIntent = new Intent(this, MainActivity.class);
        notIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent contentIntent = PendingIntent.getActivity(this, 0, notIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        RemoteViews compatView = getNotificationCompatView(isPlaying());

        mCurrentNotificationCompat.setContentIntent(contentIntent)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setWhen(System.currentTimeMillis())
                .setContent(compatView);

        Intent intent = new Intent(getApplicationContext(), MediaService.class);
        intent.setAction(MediaManager.MEDIA_STOP_ACTION);
        mCurrentNotificationCompat.setDeleteIntent(PendingIntent.getService(this, 1, intent, 0));

        Notification notification = mCurrentNotificationCompat.build();

        if (Build.VERSION.SDK_INT >= 16) {
            RemoteViews expandView = getNotificationExpandedView(isPlaying());
            notification.bigContentView = expandView;
            mNotificationManager.notify(MediaManager.MEDIA_NOTIFICATION_ID, notification);
            Picasso.with(getApplicationContext()).load(mSong.image).into(expandView,
                    R.id.notification_media_cover,
                    MediaManager.MEDIA_NOTIFICATION_ID,
                    notification);
        } else {
            mNotificationManager.notify(MediaManager.MEDIA_NOTIFICATION_ID, notification);
        }

        Picasso.with(getApplicationContext()).load(mSong.image).into(compatView,
                R.id.notification_media_cover,
                MediaManager.MEDIA_NOTIFICATION_ID,
                notification);

        startForeground(MediaManager.MEDIA_NOTIFICATION_ID, notification);
    }

    private void showCancelableMediaNotification() {
        Intent notIntent = new Intent(this, MainActivity.class);
        notIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent contentIntent = PendingIntent.getActivity(this, 0, notIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        RemoteViews compatView = getNotificationCompatView(isPlaying());

        mCurrentNotificationCompat.setContentIntent(contentIntent)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setWhen(System.currentTimeMillis())
                .setOngoing(false)
                .setContent(compatView);

        Intent intent = new Intent(getApplicationContext(), MediaService.class);
        intent.setAction(MediaManager.MEDIA_STOP_ACTION);
        mCurrentNotificationCompat.setDeleteIntent(PendingIntent.getService(this, 1, intent, 0));

        Notification notification = mCurrentNotificationCompat.build();

        if (Build.VERSION.SDK_INT >= 16) {
            RemoteViews expandView = getNotificationExpandedView(isPlaying());
            notification.bigContentView = expandView;
            mNotificationManager.notify(MediaManager.MEDIA_NOTIFICATION_ID, notification);
            Picasso.with(getApplicationContext()).load(mSong.image).into(expandView,
                    R.id.notification_media_cover,
                    MediaManager.MEDIA_NOTIFICATION_ID,
                    notification);
        } else {
            mNotificationManager.notify(MediaManager.MEDIA_NOTIFICATION_ID, notification);
        }

        Picasso.with(getApplicationContext()).load(mSong.image).into(compatView,
                R.id.notification_media_cover,
                MediaManager.MEDIA_NOTIFICATION_ID,
                notification);
    }

    private RemoteViews getNotificationExpandedView(final boolean isPlaying) {
        RemoteViews customView = new RemoteViews(getPackageName(), R.layout.view_notification_media);
        if (mSong != null) {
            customView.setTextViewText(R.id.notification_media_title, mSong.title);
        }

        if (isPlaying) {
            customView.setImageViewResource(R.id.ib_play_pause, R.drawable.ic_pause_white);
        } else {
            customView.setImageViewResource(R.id.ib_play_pause, R.drawable.ic_play_arrow_white);
        }

        Intent intent = new Intent(getApplicationContext(), MediaService.class);

        intent.setAction(MediaManager.MEDIA_PLAY_PAUSE_ACTION);
        PendingIntent pendingIntent = PendingIntent.getService(this, 1, intent, 0);
        customView.setOnClickPendingIntent(R.id.ib_play_pause, pendingIntent);

        intent.setAction(MediaManager.MEDIA_FAST_FORWARD_ACTION);
        pendingIntent = PendingIntent.getService(this, 1, intent, 0);
        customView.setOnClickPendingIntent(R.id.ib_fast_forward, pendingIntent);

        intent.setAction(MediaManager.MEDIA_REWIND_ACTION);
        pendingIntent = PendingIntent.getService(this, 1, intent, 0);
        customView.setOnClickPendingIntent(R.id.ib_rewind, pendingIntent);

        return customView;
    }

    private RemoteViews getNotificationCompatView(final boolean isPlaying) {
        RemoteViews customView = new RemoteViews(getPackageName(), R.layout.view_notification_compat_media);
        if (mSong != null) {
            customView.setTextViewText(R.id.notification_media_title, mSong.title);
        }

        if (isPlaying) {
            customView.setImageViewResource(R.id.ib_play_pause, R.drawable.ic_pause_white);
        } else {
            customView.setImageViewResource(R.id.ib_play_pause, R.drawable.ic_play_arrow_white);
        }

        Intent intent = new Intent(getApplicationContext(), MediaService.class);

        intent.setAction(MediaManager.MEDIA_PLAY_PAUSE_ACTION);
        PendingIntent pendingIntent = PendingIntent.getService(this, 1, intent, 0);
        customView.setOnClickPendingIntent(R.id.ib_play_pause, pendingIntent);

        intent.setAction(MediaManager.MEDIA_FAST_FORWARD_ACTION);
        pendingIntent = PendingIntent.getService(this, 1, intent, 0);
        customView.setOnClickPendingIntent(R.id.ib_fast_forward, pendingIntent);

        intent.setAction(MediaManager.MEDIA_REWIND_ACTION);
        pendingIntent = PendingIntent.getService(this, 1, intent, 0);
        customView.setOnClickPendingIntent(R.id.ib_rewind, pendingIntent);

        return customView;
    }

    private void updateMediaNotification() {
        RemoteViews compatView = getNotificationCompatView(isPlaying());
        RemoteViews expandView = getNotificationExpandedView(isPlaying());

        Notification notification = mCurrentNotificationCompat.setContent(compatView).build();
        notification.bigContentView = expandView;

        mNotificationManager.notify(MediaManager.MEDIA_NOTIFICATION_ID, notification);
    }

    public void setSong(Song newSong) {
        mSong = newSong;
    }

    public void playSong() {
        // Player
        if (mPlayer.isPlaying()) {
            try {
                mPlayer.pause();
            } catch (IllegalStateException e) {
                e.printStackTrace();
            }
        }
        try {
            mPlayer.stop();
        } catch (IllegalStateException e) {
            e.printStackTrace();
        }

        try {
            mPlayer.reset();
            mPlayer.setDataSource(String.valueOf(Uri.parse(mSong.file)));
            mPlayer.prepareAsync();
        }  catch (Exception e) {
            e.printStackTrace();
        }
    }

    public int getCurrentPosition() {
        return mPlayer.getCurrentPosition();
    }

    public int getDuration() {
        return mPlayer.getDuration();
    }

    public boolean isPlaying(){
        return mPlayer.isPlaying();
    }

    public void pausePlayer(){
        // Player
        try {
            mPlayer.pause();
        } catch (IllegalStateException e) {
            e.printStackTrace();
        }

        // Notification
        if (Build.VERSION.SDK_INT >= 21) {
            updateMediaNotification();
            stopForeground(false);
        } else {
            stopForeground(true);
            showCancelableMediaNotification();
        }

        Intent i = new Intent();
        i.setAction(MediaManager.MEDIA_PLAY_PAUSE_ACTION);
        i.putExtra("playing", false);
        mBroadcastManager.sendBroadcast(i);
    }

    public void startPlayer() {
        // Player
        try {
            mPlayer.start();
        } catch (IllegalStateException e) {
            e.printStackTrace();
        }

        // Notification
        showMediaNotification();

        Intent i = new Intent();
        i.setAction(MediaManager.MEDIA_PLAY_PAUSE_ACTION);
        i.putExtra(MediaManager.MEDIA_PLAY_EXTRA, true);
        mBroadcastManager.sendBroadcast(i);
    }

    public void stopPlayer() {
        mIsStoped = true;

        // Player
        if (mPlayer.isPlaying()) {
            try {
                mPlayer.pause();
            } catch (IllegalStateException e) {
                e.printStackTrace();
            }
        }

        // Player
        try {
            mPlayer.stop();
            mPlayer.reset();
        } catch (IllegalStateException e) {
            e.printStackTrace();
        }

        // Notification
        stopForeground(true);

        Intent i = new Intent();
        i.setAction(MediaManager.MEDIA_STOP_ACTION);
        i.putExtra(MediaManager.MEDIA_PLAY_EXTRA, false);
        mBroadcastManager.sendBroadcast(i);
    }

    public void seek(int pos) {
        try {
            mPlayer.seekTo(pos);
        } catch (IllegalStateException e) {
            e.printStackTrace();
        }
    }

    public void fastForward() {
        if (mPlayer != null && isPlaying()) {
            seek(Math.min(mPlayer.getCurrentPosition() + 10000, getDuration())); // +10 sec
        }
    }

    public void fastRewind() {
        if (mPlayer != null && isPlaying()) {
            seek(Math.max(mPlayer.getCurrentPosition() - 10000, 0)); // -10 sec
        }
    }

    public Song getSong() {
        return mSong;
    }

    public boolean isPlayedOnce() {
        return mIsPlayed;
    }

    public boolean isStoped() {
        return mIsStoped;
    }
}
