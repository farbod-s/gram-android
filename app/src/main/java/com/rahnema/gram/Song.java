package com.rahnema.gram;

/**
 * Created by farbod on 6/28/2017 AD.
 */

public class Song {
    public int id;
    public String image;
    public String title;
    public String singer;
    public String file;
    public boolean status; // play - pause

    public Song() {
        // nothing!
    }

    public Song(int id, String image, String title, String singer, String file, boolean status) {
        this.id = id;
        this.image = image;
        this.title = title;
        this.singer = singer;
        this.file = file;
        this.status = status;
    }

    @Override
    public String toString() {
        return "Song{" +
                "id=" + id +
                ", image='" + image + '\'' +
                ", title='" + title + '\'' +
                ", singer='" + singer + '\'' +
                ", file='" + file + '\'' +
                ", status='" + status + '\'' +
                '}';
    }
}
