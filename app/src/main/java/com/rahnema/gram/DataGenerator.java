package com.rahnema.gram;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.List;

/**
 * Created by farbod on 6/28/2017 AD.
 */

public class DataGenerator {
    public static List<Song> getSongs() {
        Gson gson = new Gson();
        Type type = new TypeToken<List<Song>>() {}.getType();
        return gson.fromJson("[{\"id\":1,\"image\":\"http://lorempixel.com/200/200/\",\"title\":\"عنوان ۱\",\"singer\":\"خواننده ۱\",\"file\":\"http://dl.nex1music.ir/1396/04/06/Behnam%20Bani%20-%20Bassame%20[128].mp3\"},{\"id\":2,\"image\":\"http://lorempixel.com/200/200/\",\"title\":\"عنوان ۲\",\"singer\":\"خواننده ۲\",\"file\":\"http://dl.nex1music.ir/1396/04/06/Mohammad%20Bahram%20-%20Zendegimi%20To%20[128].mp3\"},{\"id\":3,\"image\":\"http://lorempixel.com/200/200/\",\"title\":\"عنوان ۳\",\"singer\":\"خواننده ۳\",\"file\":\"http://dl.nex1music.ir/1396/04/06/Behnam%20Bani%20-%20Bassame%20[128].mp3\"},{\"id\":4,\"image\":\"http://lorempixel.com/200/200/\",\"title\":\"عنوان ۴\",\"singer\":\"خواننده ۴\",\"file\":\"http://dl.nex1music.ir/1396/04/06/Mohammad%20Bahram%20-%20Zendegimi%20To%20[128].mp3\"},{\"id\":5,\"image\":\"http://lorempixel.com/200/200/\",\"title\":\"عنوان ۵\",\"singer\":\"خواننده ۵\",\"file\":\"http://dl.nex1music.ir/1396/04/06/Behnam%20Bani%20-%20Bassame%20[128].mp3\"},{\"id\":6,\"image\":\"http://lorempixel.com/200/200/\",\"title\":\"عنوان ۶\",\"singer\":\"خواننده ۶\",\"file\":\"http://dl.nex1music.ir/1396/04/06/Mohammad%20Bahram%20-%20Zendegimi%20To%20[128].mp3\"},{\"id\":7,\"image\":\"http://lorempixel.com/200/200/\",\"title\":\"عنوان ۷\",\"singer\":\"خواننده ۷\",\"file\":\"http://dl.nex1music.ir/1396/04/06/Behnam%20Bani%20-%20Bassame%20[128].mp3\"},{\"id\":8,\"image\":\"http://lorempixel.com/200/200/\",\"title\":\"عنوان ۸\",\"singer\":\"خواننده ۸\",\"file\":\"http://dl.nex1music.ir/1396/04/06/Mohammad%20Bahram%20-%20Zendegimi%20To%20[128].mp3\"},{\"id\":9,\"image\":\"http://lorempixel.com/200/200/\",\"title\":\"عنوان ۹\",\"singer\":\"خواننده ۹\",\"file\":\"http://dl.nex1music.ir/1396/04/06/Behnam%20Bani%20-%20Bassame%20[128].mp3\"},{\"id\":10,\"image\":\"http://lorempixel.com/200/200/\",\"title\":\"عنوان ۱۰\",\"singer\":\"خواننده ۱۰\",\"file\":\"http://dl.nex1music.ir/1396/04/06/Mohammad%20Bahram%20-%20Zendegimi%20To%20[128].mp3\"}]", type);
    }
}
