package com.rahnema.gram;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by farbod on 6/28/2017 AD.
 */

public class SongLinearAdapter extends RecyclerView.Adapter<SongLinearAdapter.SongViewHolder> {
    private List<Song> mDataset;
    private OnSongClickedListener mListener;

    public interface OnSongClickedListener {
        void onLinearSongClicked(View view, Song song);
    }

    // Constructor
    public SongLinearAdapter(OnSongClickedListener listener) {
        mDataset = new ArrayList<>();
        mListener = listener;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public SongLinearAdapter.SongViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_song, parent, false);
        return new SongLinearAdapter.SongViewHolder(v);
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(final SongLinearAdapter.SongViewHolder holder, final int position) {
        final Song mCurrentSong = mDataset.get(position);

        Picasso.with(holder.image.getContext()).load(mCurrentSong.image).into(holder.image);
        holder.title.setText(mCurrentSong.title);
        holder.singer.setText(mCurrentSong.singer);
        holder.status.setVisibility(mCurrentSong.status ? View.VISIBLE : View.GONE);
    }

    // Return the size of dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    public void add(Song item) {
        int position = getItemCount();
        mDataset.add(position, item);
        notifyItemInserted(position);
    }

    public void add(int position, Song item) {
        mDataset.add(position, item);
        notifyItemInserted(position);
    }

    public void remove(Song item) {
        int position = mDataset.indexOf(item);
        mDataset.remove(position);
        notifyItemRemoved(position);
    }

    public void remove(int position) {
        mDataset.remove(position);
        notifyItemRemoved(position);
    }

    public void update(Song item) {
        int position = -1;
        for (int i = 0; i < getItemCount(); ++i) {
            if (mDataset.get(i).id == item.id) {
                position = i;
                break;
            }
        }

        if (position > -1)
        mDataset.set(position, item);
        notifyItemChanged(position);
    }

    public void clear() {
        final int size = mDataset.size();
        mDataset.clear();
        notifyItemRangeRemoved(0, size);
    }

    // View Holder Class
    public class SongViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.item_song_image) ImageView image;
        @BindView(R.id.item_song_title) TextView title;
        @BindView(R.id.item_song_singer) TextView singer;
        @BindView(R.id.item_song_status) ImageView status;

        public SongViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);

            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mListener.onLinearSongClicked(v, mDataset.get(getAdapterPosition()));
                }
            });
        }
    }
}