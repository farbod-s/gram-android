package com.rahnema.gram;

import android.annotation.TargetApi;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.NavigationView;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.format.DateUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import jp.wasabeef.picasso.transformations.BlurTransformation;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener,
        SongGridAdapter.OnSongClickedListener,
        SongLinearAdapter.OnSongClickedListener {

    static {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }

    private static final long PROGRESS_UPDATE_INTERNAL = 1000;
    private static final long PROGRESS_UPDATE_INITIAL_INTERVAL = 100;

    private MediaService mMediaService;
    private boolean mMediaBound = false;
    private boolean mReceiversRegistered = false;

    @BindView(R.id.toolbar) Toolbar mToolbar;
    @BindView(R.id.drawer_layout) DrawerLayout mDrawerLayout;

    @BindView(R.id.bottom_navigation) BottomNavigationView mBottomNavigationView;
    @BindView(R.id.nav_view) NavigationView mNavigationView;
    @BindView(R.id.playlist) RecyclerView mSongList;
    @BindView(R.id.bottom_sheet) View mBottomSheet;
    @BindView(R.id.placeholder) View mPlaceholder;

    // player
    @BindView(R.id.player_cover) CircleImageView mPlayerCover;
    @BindView(R.id.player_song_text) TextView mPlayerSongText;
    @BindView(R.id.player_singer_text) TextView mPlayerSingerText;
    @BindView(R.id.player_seekbar) CircularSeekBar mPlayerSeekbar;
    @BindView(R.id.player_fast_forward_button) ImageView mPlayerFastForwardButton;
    @BindView(R.id.player_fast_rewind_button) ImageView mPlayerFastRewindButton;
    @BindView(R.id.player_shuffle_button) ImageView mPlayerShuffleButton;
    @BindView(R.id.player_playpause_button) ImageView mPlayerPlayPauseButton;
    @BindView(R.id.player_background_image) ImageView mPlayerBackgroundImage;

    // playback
    @BindView(R.id.playback_main_view) View mPlaybackView;
    @BindView(R.id.playback_nav_image) ImageView mPlaybackNavButton;
    @BindView(R.id.playback_status_image) ImageView mPlaybackStatusButton;
    @BindView(R.id.playback_singer_text) TextView mPlaybackSingerText;
    @BindView(R.id.playback_song_text) TextView mPlaybackSongText;
    @BindView(R.id.player_timer_text) TextView mPlayerTimerText;
    @BindView(R.id.playback_line_view) View mPlaybackLineView;

    private SongListFragment mSongListFragment;
    private AboutFragment mAboutFragment;
    private OtherFragment mOtherFragment;

    private RecyclerView.LayoutManager mLayoutManager;
    private DividerItemDecoration mItemDecoration;
    private SongLinearAdapter mSongAdapter;

    private Song mSong;

    private final Handler mHandler = new Handler();

    private final Runnable mUpdateProgressTask = new Runnable() {
        @Override
        public void run() {
            updateProgress();
        }
    };

    private final ScheduledExecutorService mExecutorService =
            Executors.newSingleThreadScheduledExecutor();

    private ScheduledFuture<?> mScheduleFuture;

    private ServiceConnection mMediaConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName className, IBinder service) {
            MediaService.MediaBinder binder = (MediaService.MediaBinder) service;
            mMediaService = binder.getService();
            mMediaBound = true;

            mSong = mMediaService.getSong();
            updatePlayer(mMediaService.isPlaying());
            updateProgress();
            togglePlaybackVisibility(mMediaService.isPlayedOnce());
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            mMediaBound = false;
        }
    };

    private final BroadcastReceiver mMediaReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(MediaManager.MEDIA_PLAY_PAUSE_ACTION)) {
                boolean isPlaying = intent.getBooleanExtra(MediaManager.MEDIA_PLAY_EXTRA, false);
                updatePlayer(isPlaying);
            } else if (intent.getAction().equals(MediaManager.MEDIA_STOP_ACTION)) {
                updatePlayer(false);
            } else if (intent.getAction().equals(MediaManager.MEDIA_START_ACTION)) {
                updatePlayer(true);
            }
        }
    };

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        setSupportActionBar(mToolbar);

        // bottom sheet & drawer
        setupNavigations();

        // Add product list fragment if this is first creation
        if (savedInstanceState == null) {
            mSongListFragment = new SongListFragment();
            mAboutFragment = new AboutFragment();
            mOtherFragment = new OtherFragment();

            setupBottomNavigation();
        }
        updateNavigationState();

        // player
        setupPlayer();

        // RTL
        forceRTLIfSupported();
    }

    @Override
    protected void onStart() {
        super.onStart();

        Intent mediaIntent = new Intent(this, MediaService.class);
        bindService(mediaIntent, mMediaConnection, Context.BIND_AUTO_CREATE);

        registerReceiver();
    }

    @Override
    protected void onStop() {
        super.onStop();

        if (mMediaBound) {
            unbindService(mMediaConnection);
            mMediaBound = false;
        }

        unregisterReceiver();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        stopSeekbarUpdate();
        mExecutorService.shutdown();
    }

    public void playSong(Song song) {
        if (mMediaBound) {
            mMediaService.setSong(song);

            Intent i = new Intent(this, MediaService.class);
            i.setAction(MediaManager.MEDIA_START_ACTION);
            startService(i);
        }
    }

    @Override
    public void onBackPressed() {
        if (mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
            mDrawerLayout.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_search) {
            // TODO - show search stuff
            Toast.makeText(this, "Search clicked!", Toast.LENGTH_SHORT).show();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_about) {
            // Handle the about action
            if (mAboutFragment != null) {
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.placeholder, mAboutFragment)
                        .commit();
            }
        }

        mDrawerLayout.closeDrawer(GravityCompat.START);
        return true;
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    private void forceRTLIfSupported() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
        }
    }

    private void setupNavigations() {
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, mDrawerLayout, mToolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        mDrawerLayout.setDrawerListener(toggle);
        toggle.syncState();

        mNavigationView.setNavigationItemSelectedListener(this);

        BottomSheetBehavior behavior = BottomSheetBehavior.from(mBottomSheet);
        behavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                if (newState == BottomSheetBehavior.STATE_COLLAPSED) {
                    mBottomNavigationView.setVisibility(View.VISIBLE);
                    toggleBottomSheetHandler(true);
                } else {
                    mBottomNavigationView.setVisibility(View.GONE);
                    toggleBottomSheetHandler(false);
                }

                if (newState == BottomSheetBehavior.STATE_EXPANDED) {
                    mPlaceholder.setVisibility(View.GONE);
                    mToolbar.setVisibility(View.GONE);
                    mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
                } else {
                    mPlaceholder.setVisibility(View.VISIBLE);
                    mToolbar.setVisibility(View.VISIBLE);
                    mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
                }
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {
                // TODO - do fading stuff here
            }
        });
    }

    private void setupPlayer() {
        // seekbar
        mPlayerSeekbar.setOnSeekBarChangeListener(new CircularSeekBar.OnCircularSeekBarChangeListener() {
            @Override
            public void onProgressChanged(CircularSeekBar circularSeekBar, int progress, boolean fromUser) {
                mPlayerTimerText.setText(DateUtils.formatElapsedTime(progress));
            }

            @Override
            public void onStopTrackingTouch(CircularSeekBar seekBar) {
                if (mMediaBound) {
                    mMediaService.seek(mPlayerSeekbar.getProgress() * 1000);
                    scheduleSeekbarUpdate();
                }
            }

            @Override
            public void onStartTrackingTouch(CircularSeekBar seekBar) {
                stopSeekbarUpdate();
            }
        });

        mPlayerFastForwardButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mMediaBound) {
                    if (mMediaService.isPlaying()) {
                        mMediaService.fastForward();
                    }
                }
            }
        });

        mPlayerFastRewindButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mMediaBound) {
                    if (mMediaService.isPlaying()) {
                        mMediaService.fastRewind();
                    }
                }
            }
        });

        mPlayerPlayPauseButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mMediaBound) {
                    if (mMediaService.isPlaying()) {
                        mMediaService.pausePlayer();
                    } else {
                        if (mMediaService.getSong() == null || mMediaService.isStoped()) {
                            playSong(mSong);
                        } else {
                            mMediaService.startPlayer();
                        }
                    }
                }
            }
        });

        mPlayerShuffleButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO - shuffle stuff here
            }
        });

        // song list
        setupSongList();
    }

    private void setupSongList() {
        mSongAdapter = new SongLinearAdapter(this);
        mLayoutManager = new LinearLayoutManager(this);
        mItemDecoration = new DividerItemDecoration(this, getResources().getColor(R.color.divider), 1);


        mSongList.setLayoutManager(mLayoutManager);
        mSongList.addItemDecoration(mItemDecoration);
        mSongList.setHasFixedSize(true);
        mSongList.setNestedScrollingEnabled(false);
        mSongList.setAdapter(mSongAdapter);

        // dummy data
        List<Song> songs = DataGenerator.getSongs();
        for (int i = 0; i < songs.size(); ++i) {
            mSongAdapter.add(songs.get(i));
        }
    }

    private void updateNavigationState() {
        Menu menu = mBottomNavigationView.getMenu();
        if (menu.size() > 0) {
            MenuItem item = menu.getItem(0);
            item.setChecked(item.getItemId() == R.id.action_3);
        }

        // update main content
        if (mSongListFragment != null) {
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.placeholder, mSongListFragment)
                    .commit();
        }
    }

    private void setupBottomNavigation() {
        mBottomNavigationView.setOnNavigationItemSelectedListener(
                new BottomNavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                        //uncheckNavigationDrawerItems();

                        switch (item.getItemId()) {
                            case R.id.action_1:
                                getSupportFragmentManager().beginTransaction()
                                        .replace(R.id.placeholder, mSongListFragment)
                                        .commit();
                                return true;
                            case R.id.action_2:
                                getSupportFragmentManager().beginTransaction()
                                        .replace(R.id.placeholder, mOtherFragment)
                                        .commit();
                                return true;
                            case R.id.action_3:
                                getSupportFragmentManager().beginTransaction()
                                        .replace(R.id.placeholder, mOtherFragment)
                                        .commit();
                                return true;
                        }
                        return false;
                    }
                }
        );
    }

    @Override
    public void onGridSongClicked(View view, Song song) {
        song.status = true;
        if (mSong != null) {
            mSong.status = false;
            mSongAdapter.update(mSong);
        }

        mSong = song;
        playSong(mSong);
        togglePlaybackVisibility(true);
    }

    @Override
    public void onLinearSongClicked(View view, Song song) {
        song.status = true;
        if (mSong != null) {
            mSong.status = false;
            mSongAdapter.update(mSong);
        }

        mSong = song;
        playSong(mSong);
        togglePlaybackVisibility(true);
    }

    private void scheduleSeekbarUpdate() {
        stopSeekbarUpdate();
        if (!mExecutorService.isShutdown()) {
            mScheduleFuture = mExecutorService.scheduleAtFixedRate(
                    new Runnable() {
                        @Override
                        public void run() {
                            mHandler.post(mUpdateProgressTask);
                        }
                    }, PROGRESS_UPDATE_INITIAL_INTERVAL,
                    PROGRESS_UPDATE_INTERNAL, TimeUnit.MILLISECONDS);
        }
    }

    private void stopSeekbarUpdate() {
        if (mScheduleFuture != null) {
            mScheduleFuture.cancel(false);
        }
    }

    private void updateProgress() {
        if (mMediaBound) {
            int position = mMediaService.getCurrentPosition();
            mPlayerSeekbar.setProgress(position / 1000);
        }
    }

    private void registerReceiver() {
        if (!mReceiversRegistered) {
            IntentFilter i = new IntentFilter();
            i.addAction(MediaManager.MEDIA_PLAY_PAUSE_ACTION);
            i.addAction(MediaManager.MEDIA_START_ACTION);
            i.addAction(MediaManager.MEDIA_STOP_ACTION);
            LocalBroadcastManager.getInstance(this).registerReceiver(mMediaReceiver, i);
            mReceiversRegistered = true;
        }
    }

    private void unregisterReceiver() {
        if (mReceiversRegistered) {
            LocalBroadcastManager.getInstance(this).unregisterReceiver(mMediaReceiver);
            mReceiversRegistered = false;
        }
    }

    private void toggleBottomSheetHandler(boolean shown) {
        if (!shown) {
            mPlaybackView.setBackgroundColor(getResources().getColor(R.color.transparent));
            mPlaybackStatusButton.setVisibility(View.GONE);
            mPlaybackSongText.setVisibility(View.GONE);
            mPlaybackLineView.setVisibility(View.GONE);
            mPlaybackSingerText.setVisibility(View.GONE);
            mPlaybackNavButton.setImageDrawable(getResources().getDrawable(R.drawable.ic_keyboard_arrow_down_black_24dp));
            DrawableCompat.setTint(mPlaybackNavButton.getDrawable(), ContextCompat.getColor(this, R.color.buttonSecondary));
        } else {
            mPlaybackView.setBackgroundColor(getResources().getColor(R.color.colorAccent));
            mPlaybackStatusButton.setVisibility(View.VISIBLE);
            mPlaybackSongText.setVisibility(View.VISIBLE);
            mPlaybackLineView.setVisibility(View.VISIBLE);
            mPlaybackSingerText.setVisibility(View.VISIBLE);
            mPlaybackNavButton.setImageDrawable(getResources().getDrawable(R.drawable.ic_keyboard_arrow_up_black_24dp));
            DrawableCompat.setTint(mPlaybackNavButton.getDrawable(), ContextCompat.getColor(this, R.color.colorPrimary));
        }
    }

    private void updatePlayer(boolean isPlaying) {
        if (mSong != null) {
            mSong.status = isPlaying;
            mSongAdapter.update(mSong);
        }

        if (isPlaying) {
            scheduleSeekbarUpdate();
            mPlayerPlayPauseButton.setImageDrawable(getResources().getDrawable(R.drawable.ic_pause_white_24dp));
        } else {
            stopSeekbarUpdate();
            mPlayerPlayPauseButton.setImageDrawable(getResources().getDrawable(R.drawable.ic_play_arrow_white_24dp));
        }
    }

    private void togglePlaybackVisibility(boolean shown) {
        if (shown) {
            // playback
            mPlaybackSingerText.setText(mSong.singer);
            mPlaybackSongText.setText(mSong.title);

            // player
            Picasso.with(this).load(mSong.image).into(mPlayerCover);
            mPlayerSongText.setText(mSong.title);
            mPlayerSingerText.setText(mSong.singer);

            // blurred bg
            Picasso.with(this)
                    .load(mSong.image)
                    .placeholder(R.color.white)
                    .transform(new BlurTransformation(this))
                    .into(mPlayerBackgroundImage);

            CoordinatorLayout.LayoutParams layoutParams = new CoordinatorLayout.LayoutParams(
                    CoordinatorLayout.LayoutParams.MATCH_PARENT, CoordinatorLayout.LayoutParams.MATCH_PARENT);

            layoutParams.setMargins(0, (int) Converter.convertDpToPixel(56, this), 0, (int) Converter.convertDpToPixel(104, this));
            mPlaceholder.setLayoutParams(layoutParams);
            mBottomSheet.setVisibility(View.VISIBLE);
        } else {
            // TODO - hide playback
        }
    }
}
