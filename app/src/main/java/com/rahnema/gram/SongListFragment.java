package com.rahnema.gram;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by farbod on 6/28/2017 AD.
 */

public class SongListFragment extends Fragment {

    @BindView(R.id.list) RecyclerView mRecyclerView;

    private RecyclerView.LayoutManager mLayoutManager;
    private SongGridAdapter mAdapter;

    private Context mContext;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        this.mContext = getActivity();

        mLayoutManager = new GridLayoutManager(mContext, 3);
        mAdapter = new SongGridAdapter((SongGridAdapter.OnSongClickedListener) mContext);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_list, parent, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        ButterKnife.bind(this, view);

        setupRecyclerView();

        // dummy data
        List<Song> songs = DataGenerator.getSongs();
        for (int i = 0; i < songs.size(); ++i) {
            mAdapter.add(songs.get(i));
        }
    }

    private void setupRecyclerView() {
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setAdapter(mAdapter);
    }
}
